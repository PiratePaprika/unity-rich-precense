﻿using System;
using UnityEngine;

public class DiscordTutorial : MonoBehaviour {
    private Discord.Discord _discord;
    
    private void Start() {
        _discord = new Discord.Discord(685157281419100201, (UInt64)Discord.CreateFlags.Default);
        
        var activity = new Discord.Activity
        {
            State = "In A Party",
            Details = "Coding a tutorial",
            Party =
            {
                Size = {
                    CurrentSize = 2,
                    MaxSize = 4
                }
            },
            Assets = {
                LargeImage = "lightondark",
                LargeText = "Hover Me!",
                SmallImage = "lightondark",
                SmallText = "Small image"
            },
            Instance = true
        };

        _discord.GetActivityManager().UpdateActivity(activity, (result) =>
        {
            if (result == Discord.Result.Ok)
            {
                print("Success!");
            }
            else
            {
                print("Failed");
            }
        });
    }

    private void Update() {
        _discord.RunCallbacks();
    }

    private void OnApplicationQuit() {
        _discord.Dispose();
    }
}
